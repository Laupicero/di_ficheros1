﻿namespace InfoFicheros
{
    partial class miInfo
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(miInfo));
            this.listArchivos = new System.Windows.Forms.ListBox();
            this.lblRuta = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.elegirRutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtxInfoArchivo = new System.Windows.Forms.RichTextBox();
            this.lblInfoArchivo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelName = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listArchivos
            // 
            this.listArchivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listArchivos.FormattingEnabled = true;
            this.listArchivos.ItemHeight = 16;
            this.listArchivos.Location = new System.Drawing.Point(46, 165);
            this.listArchivos.Name = "listArchivos";
            this.listArchivos.Size = new System.Drawing.Size(239, 308);
            this.listArchivos.TabIndex = 0;
            this.listArchivos.SelectedIndexChanged += new System.EventHandler(this.listArchivos_SelectedIndexChanged);
            // 
            // lblRuta
            // 
            this.lblRuta.AutoSize = true;
            this.lblRuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuta.Location = new System.Drawing.Point(43, 132);
            this.lblRuta.Name = "lblRuta";
            this.lblRuta.Size = new System.Drawing.Size(168, 17);
            this.lblRuta.TabIndex = 1;
            this.lblRuta.Text = "Aqui viene la ruta elegida";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.elegirRutaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(658, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // elegirRutaToolStripMenuItem
            // 
            this.elegirRutaToolStripMenuItem.Name = "elegirRutaToolStripMenuItem";
            this.elegirRutaToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.elegirRutaToolStripMenuItem.Text = "Elegir Ruta";
            this.elegirRutaToolStripMenuItem.ToolTipText = "Selecciona la ruta";
            this.elegirRutaToolStripMenuItem.Click += new System.EventHandler(this.elegirRutaToolStripMenuItem_Click);
            // 
            // rtxInfoArchivo
            // 
            this.rtxInfoArchivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxInfoArchivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxInfoArchivo.Location = new System.Drawing.Point(330, 165);
            this.rtxInfoArchivo.Name = "rtxInfoArchivo";
            this.rtxInfoArchivo.Size = new System.Drawing.Size(265, 303);
            this.rtxInfoArchivo.TabIndex = 3;
            this.rtxInfoArchivo.Text = "Nombre completo:  xxxxx\nDirectorio padre:  xxxx\n....\nToda la info que podais del " +
    "fichero";
            // 
            // lblInfoArchivo
            // 
            this.lblInfoArchivo.AutoSize = true;
            this.lblInfoArchivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoArchivo.Location = new System.Drawing.Point(327, 132);
            this.lblInfoArchivo.Name = "lblInfoArchivo";
            this.lblInfoArchivo.Size = new System.Drawing.Size(268, 17);
            this.lblInfoArchivo.TabIndex = 4;
            this.lblInfoArchivo.Text = "Aqui  viene el nombre del archivo elegido";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(658, 92);
            this.panel1.TabIndex = 5;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelName});
            this.statusStrip1.Location = new System.Drawing.Point(0, 499);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(658, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelName
            // 
            this.toolStripStatusLabelName.Name = "toolStripStatusLabelName";
            this.toolStripStatusLabelName.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabelName.Text = "Laura Lucena Buendia";
            // 
            // miInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(658, 521);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblInfoArchivo);
            this.Controls.Add(this.rtxInfoArchivo);
            this.Controls.Add(this.lblRuta);
            this.Controls.Add(this.listArchivos);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "miInfo";
            this.ShowIcon = false;
            this.Text = "INFORMACIÓN DE FICHEROS";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listArchivos;
        private System.Windows.Forms.Label lblRuta;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem elegirRutaToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtxInfoArchivo;
        private System.Windows.Forms.Label lblInfoArchivo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelName;
    }
}

