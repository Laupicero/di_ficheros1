﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfoFicheros
{
    public partial class miInfo : Form
    {
        private String[] RutaArchivos;
        public miInfo()
        {
            InitializeComponent();
        }


        //Botón para elegir la ruta
        private void elegirRutaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            //Almacenamos todos los ficheros
            if (result == DialogResult.OK)
            {
                RutaArchivos = Directory.GetFiles(fbd.SelectedPath);
                lblRuta.Text = fbd.SelectedPath;
            }
            //mostramos en el Listbox
            foreach (String archivo in RutaArchivos)
            {
                listArchivos.Items.Add(Path.GetFileName(archivo));
            }
        }

        
        //Cuando seleccioanmos un fichero en el listBox
        private void listArchivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblInfoArchivo.Text = listArchivos.SelectedItem.ToString();
            FileInfo mifichero = new FileInfo(getRuta(listArchivos.SelectedItem.ToString()));

            rtxInfoArchivo.Text = "\nNombre: " + mifichero.Name +
                "\n\nRuta: " + mifichero.FullName +
                "\n\nExtensión del archivo: " + mifichero.Extension +
                "\n\nDirectorio del archivo: " + mifichero.DirectoryName +
                "\n\nTamaño" + mifichero.Length +
                "\n\nCreado" + mifichero.CreationTime +
                "\n\nÚltimo Acceso: " + mifichero.LastAccessTime;
        }

        
        //Nos devuelve la ruta del item seleccionado
        private String getRuta(String nombreFich)
        {
            String rutaFich = "";
            foreach (String ruta in RutaArchivos)
            {
                if (Path.GetFileName(ruta) == nombreFich)
                    rutaFich = ruta;
            }
            return rutaFich;
        }
    }
}
